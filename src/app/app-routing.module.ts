import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverOnsComponent } from './components/pages/over-ons/over-ons.component';
import { AppleNowComponent } from './components/pages/apple-now/apple-now.component';
import { LaatJeInspirerenComponent } from './components/pages/laat-je-inspireren/laat-je-inspireren.component';
import { ComingSoonComponent } from './components/pages/coming-soon/coming-soon.component';
import { ContactComponent } from './components/pages/contact/contact.component';
import { ErrorComponent } from './components/pages/error/error.component';
import { FaqComponent } from './components/pages/faq/faq.component';
import { HomeComponent } from './components/pages/home/home.component';
import { ProjectsDetailsComponent } from './components/pages/projects-details/projects-details.component';
import { ProjectsComponent } from './components/pages/projects/projects.component';
import { TestimonialsComponent } from './components/pages/testimonials/testimonials.component';
import { PvcDetailsComponent } from './components/pages/projects-details/pvc/pvc-details.component';
import {TapijtDetailsComponent} from './components/pages/projects-details/tapijt/tapijt-details.component';
import {LaminaatDetailsComponent} from './components/pages/projects-details/laminaat/laminaat-details.component';
import {TapijtTrapDetailsComponent} from './components/pages/projects-details/tapijt-trap/tapijt-trap-details.component';
import {PvcTrapDetailsComponent} from './components/pages/projects-details/pvc-trap/pvc-trap-details.component';
import {KunstgrasDetailsComponent} from './components/pages/projects-details/kunstgras/kunstgras-details.component';
import {VinylTrapDetailsComponent} from './components/pages/projects-details/vinyl-trap/vinyl-trap-details.component';
import {KarpettenDetailsComponent} from './components/pages/projects-details/karpetten/karpetten-details.component';
import {HoutDetailsComponent} from './components/pages/projects-details/hout/hout-details.component';
import {PlooigordijnDetailsComponent} from './components/pages/projects-details/plooigordijn/plooigordijn-details.component';
import {WavegordijnDetailsComponent} from './components/pages/projects-details/wavegordijn/wavegordijn-details.component';
import {VouwgordijnDetailsComponent} from './components/pages/projects-details/vouwgordijn/vouwgordijn-details.component';
import {JaloezieenDetailsComponent} from './components/pages/projects-details/jaloezieen/jaloezieen-details.component';
import {TagwestDetailsComponent} from './components/pages/tevreden-klanten/tagwest/tagwest-details.component';
import {WoningIjsselmuidenDetailsComponent} from './components/pages/tevreden-klanten/woning-ijsselmuiden/woning-ijsselmuiden-details.component';
import {TevredenKlantenComponent} from './components/pages/tevreden-klanten/tevreden-klanten.component';
import {WoningHattemDetailsComponent} from './components/pages/tevreden-klanten/woning-hattem/woning-hattem-details.component';
import {WoningElspeetDetailsComponent} from './components/pages/tevreden-klanten/woning-elspeet/woning-elspeet-details.component';
import {WoningGenemuidenDetailsComponent} from './components/pages/tevreden-klanten/woning-genemuiden/woning-genemuiden-details.component';
import {AdvundumDetailsComponent} from './components/pages/tevreden-klanten/advundum/advundum-details.component';
import {InvictusComponent} from './components/pages/invictus/invictus.component';
import {PenthouseAmsterdamDetailsComponent} from './components/pages/tevreden-klanten/penthouse-amsterdam/penthouse-amsterdam-details.component';

const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'over-ons', component: OverOnsComponent},
    {path: 'apply-now', component: AppleNowComponent},
    {path: 'projects', component: ProjectsComponent},
    {path: 'projects-details', component: ProjectsDetailsComponent},
    {path: 'pvc-details', component: PvcDetailsComponent},
    {path: 'pvc-trap-details', component: PvcTrapDetailsComponent},
    {path: 'tapijt-details', component: TapijtDetailsComponent},
    {path: 'tapijt-trap-details', component: TapijtTrapDetailsComponent},
    {path: 'laminaat-details', component: LaminaatDetailsComponent},
    {path: 'vinyl-trap-details', component: VinylTrapDetailsComponent},
    {path: 'kunstgras-details', component: KunstgrasDetailsComponent},
    {path: 'karpetten-details', component: KarpettenDetailsComponent},
    {path: 'hout-details', component: HoutDetailsComponent},
    {path: 'plooigordijn-details', component: PlooigordijnDetailsComponent},
    {path: 'wavegordijn-details', component: WavegordijnDetailsComponent},
    {path: 'vouwgordijn-details', component: VouwgordijnDetailsComponent},
    {path: 'jaloezieen-details', component: JaloezieenDetailsComponent},
    {path: 'faq', component: FaqComponent},
    {path: 'testimonials', component: TestimonialsComponent},
    {path: 'error', component: ErrorComponent},
    {path: 'coming-soon', component: ComingSoonComponent},
    {path: 'tevreden-klanten', component: TevredenKlantenComponent},
    {path: 'laat-je-inspireren', component: LaatJeInspirerenComponent},
    {path: 'contact', component: ContactComponent},
    {path: 'tagwest-details', component: TagwestDetailsComponent},
    {path: 'hoekwoning-ijsselmuiden-details', component: WoningIjsselmuidenDetailsComponent},
    {path: 'hoekwoning-hattem-details', component: WoningHattemDetailsComponent},
    {path: 'hoekwoning-elspeet-details', component: WoningElspeetDetailsComponent},
    {path: 'vrijstaande-woning-genemuiden-details', component: WoningGenemuidenDetailsComponent},
    {path: 'advundum-details', component: AdvundumDetailsComponent},
    {path: 'invictus', component: InvictusComponent},
    {path: 'penthouse-amsterdam', component: PenthouseAmsterdamDetailsComponent},

    {path: '**', component: ErrorComponent} // This line will remain down from the whole component list
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
