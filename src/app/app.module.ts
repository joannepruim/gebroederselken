import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/pages/home/home.component';
import { PreloaderComponent } from './components/common/preloader/preloader.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { NavbarComponent } from './components/common/navbar/navbar.component';
import { LaatJeInspirerenComponent } from './components/pages/laat-je-inspireren/laat-je-inspireren.component';
import { ComingSoonComponent } from './components/pages/coming-soon/coming-soon.component';
import { ErrorComponent } from './components/pages/error/error.component';
import { TestimonialsComponent } from './components/pages/testimonials/testimonials.component';
import { FaqComponent } from './components/pages/faq/faq.component';
import { ProjectsDetailsComponent } from './components/pages/projects-details/projects-details.component';
import { PvcDetailsComponent } from './components/pages/projects-details/pvc/pvc-details.component';
import { ProjectsComponent } from './components/pages/projects/projects.component';
import { AppleNowComponent } from './components/pages/apple-now/apple-now.component';
import { OverOnsComponent } from './components/pages/over-ons/over-ons.component';
import { ContactComponent } from './components/pages/contact/contact.component';
import { TapijtDetailsComponent } from './components/pages/projects-details/tapijt/tapijt-details.component';
import { LaminaatDetailsComponent } from './components/pages/projects-details/laminaat/laminaat-details.component';
import {TapijtTrapDetailsComponent} from './components/pages/projects-details/tapijt-trap/tapijt-trap-details.component';
import {PvcTrapDetailsComponent} from './components/pages/projects-details/pvc-trap/pvc-trap-details.component';
import {KunstgrasDetailsComponent} from './components/pages/projects-details/kunstgras/kunstgras-details.component';
import {VinylTrapDetailsComponent} from './components/pages/projects-details/vinyl-trap/vinyl-trap-details.component';
import {KarpettenDetailsComponent} from './components/pages/projects-details/karpetten/karpetten-details.component';
import {HoutDetailsComponent} from './components/pages/projects-details/hout/hout-details.component';
import {PlooigordijnDetailsComponent} from './components/pages/projects-details/plooigordijn/plooigordijn-details.component';
import {WavegordijnDetailsComponent} from './components/pages/projects-details/wavegordijn/wavegordijn-details.component';
import {VouwgordijnDetailsComponent} from './components/pages/projects-details/vouwgordijn/vouwgordijn-details.component';
import {JaloezieenDetailsComponent} from './components/pages/projects-details/jaloezieen/jaloezieen-details.component';
import {DienstenComponent} from './components/pages/diensten/diensten.component';
import {TagwestDetailsComponent} from './components/pages/tevreden-klanten/tagwest/tagwest-details.component';
import {ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { WoningIjsselmuidenDetailsComponent } from './components/pages/tevreden-klanten/woning-ijsselmuiden/woning-ijsselmuiden-details.component';
import { TevredenKlantenComponent } from './components/pages/tevreden-klanten/tevreden-klanten.component';
import { WoningHattemDetailsComponent } from './components/pages/tevreden-klanten/woning-hattem/woning-hattem-details.component';
import {WoningElspeetDetailsComponent} from './components/pages/tevreden-klanten/woning-elspeet/woning-elspeet-details.component';
import {WoningGenemuidenDetailsComponent} from './components/pages/tevreden-klanten/woning-genemuiden/woning-genemuiden-details.component';
import {AdvundumDetailsComponent} from './components/pages/tevreden-klanten/advundum/advundum-details.component';
import {InvictusComponent} from './components/pages/invictus/invictus.component';
import {PenthouseAmsterdamDetailsComponent} from './components/pages/tevreden-klanten/penthouse-amsterdam/penthouse-amsterdam-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PreloaderComponent,
    FooterComponent,
    NavbarComponent,
    LaatJeInspirerenComponent,
    ComingSoonComponent,
    ErrorComponent,
    TestimonialsComponent,
    FaqComponent,
    ProjectsDetailsComponent,
    PvcDetailsComponent,
    TapijtDetailsComponent,
    ProjectsComponent,
    AppleNowComponent,
    OverOnsComponent,
    ContactComponent,
    LaminaatDetailsComponent,
    TapijtTrapDetailsComponent,
    VinylTrapDetailsComponent,
    PvcTrapDetailsComponent,
    KunstgrasDetailsComponent,
    KarpettenDetailsComponent,
    HoutDetailsComponent,
    PlooigordijnDetailsComponent,
    WavegordijnDetailsComponent,
    VouwgordijnDetailsComponent,
    JaloezieenDetailsComponent,
    DienstenComponent,
    TagwestDetailsComponent,
    WoningIjsselmuidenDetailsComponent,
    TevredenKlantenComponent,
    WoningHattemDetailsComponent,
    WoningElspeetDetailsComponent,
    WoningGenemuidenDetailsComponent,
    AdvundumDetailsComponent,
    InvictusComponent,
    PenthouseAmsterdamDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
