import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    isSuccess?: boolean;
    // successMessage?: Object;
    // errorMessage?: any;
    // response?: Object;

    contactForm = new FormGroup({
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        phoneNumber: new FormControl('',[
            Validators.required,
            Validators.minLength(10),
            Validators.maxLength(10)
        ]),
        email: new FormControl('', [
            Validators.required,
            Validators.pattern('[^ @]*@[^ @]*')
        ]),
        question: new FormControl('', Validators.required),
    });

    constructor(private readonly http: HttpClient) { }

    ngOnInit(): void {
    }


    onSubmit() {
        // Prevent default to avoid sending the original HTML form (default browser action)
        // e.preventDefault();

        // TODO: add some validation and prevent sending the form if the form still has errors
        // if (this.contactForm.invalid) {
        //     this.errorMessage = 'fout';
        // }
        // if (this.contactForm.valid) {
        //     this.successMessage = 'lekker';
        // }

        // Send the form
        this.http
            .post('https://formcarry.com/s/l_gXXt83oSf', this.contactForm.value, {
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                }
            })
            .subscribe({
                next: (res) => {
                    // this.response = res;
                    this.isSuccess = true;
                    // this.successMessage = 'Verzonden!';
                    this.contactForm.reset();
                },
                error: () => {
                    // this.errorMessage = e;
                }
            });
    }


}
